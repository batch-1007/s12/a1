/* FizzBuzz*/

function fizzBuzz(num) {
	if (num % 3 == 0 && num % 5 == 0){
		return "FizzBuzz";
	}else if (num % 5 == 0){
		return ("Buzz");
	}else if(num % 3 ==0){
		return "Fizz";
	}else {
		return "pop";
	}
}
console.log(fizzBuzz(5));
console.log(fizzBuzz(30));
console.log(fizzBuzz(27));
console.log(fizzBuzz(17));





/*Roygbiv*/

function color(letter){
	let cleanedLetter = letter.toLowerCase();

	if (cleanedLetter === "r"){
		return "Red";
	}else if (cleanedLetter === "o"){
		return "Orange";
	}else if (cleanedLetter === "y"){
		return "Yellow";
	}else if (cleanedLetter === "g"){
		return "Green";
	}else if (cleanedLetter === "b"){
		return	"Blue";
	}else if (cleanedLetter === "i"){
		return "Indigo";
	}else if (cleanedLetter === "v"){
		return ("Violet");
	}else{
		return " No color";
	}
}

//------test-------
console.log(color("r"));
console.log(color("G"));
console.log(color("x"));
console.log(color("B"));
console.log(color("i"));




/*Leap year*/

function leapYear(year){
	if (year % 4 == 0){
		if (year % 100 == 0){
			if (year % 400 == 0){
				return "Leap Year";
			}else{
				return "Not a Leap year";
			}
		}else{
			return "leap year";
		}
	}else{
		return "Not leap year";
	}
}

//------test-------
console.log(leapYear(1900));
console.log(leapYear(2000));
console.log(leapYear(2004));
console.log(leapYear(2021));
